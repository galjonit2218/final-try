const PORT = process.env.PORT || 3000
const express = require('express')

var app = express()

app.use('/',(req, res) => {
    res.send('App works!!')
})

app.listen(PORT, (err) => {
    if(err)
        console.log(err)
    else
        console.log(`Aha!!, server is running on port ${PORT}`)
})